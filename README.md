# monitoring Prometheus/grafana

Solution de monitoring pour les conteneurs Docker avec [Prometheus](https://prometheus.io/), [Grafana](http://grafana.org/), [cAdvisor](https://github.com/google/cadvisor),
[NodeExporter](https://github.com/prometheus/node_exporter) et les alertes avec [AlertManager](https://github.com/prometheus/alertmanager).

Pour cela nous allons utiliser une solution packagée et configurée pour superviser nos conteneurs Docker avec un tableau de bord simpe mais efficace pour suivre les "metrics" de nos conteneurs et de s'assurer de la bonne santé de l'hôte.

## Installation 

Prè-requis:

* Docker Engine
* Docker Compose

On récupère le projet.
``` bash
git clone https://gitlab.com/Morgan59181/prometheus_grafana_pour_docker.git
cd prometheus_grafana_pour_docker # vous pouvez modifier le nom du dossier 
```
On lance le docker-compose
``` bash
docker-compose up -d
```
On verifie que les conteneurs sont lancés avec :
``` bash
docker-compose ps
```

Conteneurs:

* Prometheus (base de données métriques) http://localhost:9090
* Prometheus-Pushgateway (push pour les travaux éphémères et par lots) http://localhost:9091
* AlertManager (gestion des alertes) http://localhost:9093
* Grafana (visualiser les métriques) http://localhost:3000
* NodeExporter (collecteur de métriques hôtes)
* cAdvisor (collecteur de métriques de conteneurs)
* Caddy (proxy inverse et fournisseur d’authentification de base pour prometheus et alertmanager)

## Configuration Grafana

Une fois que tous les conteneurs sont démarrés, ouvrir une session sur Grafana à l'adresse : http://localhost:3000

Par défaut, les identifiants sont :
 * user : ***admin***   
 * password : ***admin*** 

Vous pouvez modifier les informations d’identification dans le fichier `config`
ou en fournissant les variables d'environnement `ADMIN_USER` and `ADMIN_PASSWORD` dans le docker-compose up.

Préciser `env_file` dans le docker-compose

```yaml
grafana:
  image: grafana/grafana:8.1.5
  env_file:
    - config
```
Syntax du fichier `config`

```yaml
GF_SECURITY_ADMIN_USER=admin
GF_SECURITY_ADMIN_PASSWORD=changeme
GF_USERS_ALLOW_SIGN_UP=false
```
Pour que la modification du mot de passe soit effective, vous devez supprimer cette entrée :

```yaml
- grafana_data:/var/lib/grafana
```

Une fois connecté, Grafana est préconfiguré avec deux tableaux de bord et Prometheus comme source de données par défaut :

* Nom : Prometheus
* Type : Prometheus
* URL : http://prometheus:9090
* Accès : proxy

***Docker Host Dashboard***

Le tableau de bord de l’hôte Docker affiche des mesures clés pour surveiller l’utilisation des ressources de votre serveur :

* Disponibilité du serveur, pourcentage d’inactivité du processeur, nombre de cœurs de processeur, mémoire disponible, échange et stockage
* Graphique de charge moyenne du système, en cours d’exécution et bloqué par le graphique des processus d’E/S, graphique des interruptions
* Graphique d’utilisation du processeur par mode (invité, inactif, iowait, irq, nice, softirq, steal, système, utilisateur)
* Graphique d’utilisation de la mémoire par distribution (utilisé, libre, tampons, mis en cache)
* Graphique d’utilisation des E/S (lire Bps, lire Bps et temps d’E/S)
* Graphique d’utilisation du réseau par périphérique (Bps entrant, Bps sortant)
* Permuter les graphiques d’utilisation et d’activité

/!\ Pour le graphique de stockage libre, vous devez spécifier le fstype dans la demande de graphe de grafana. Il faut modifier la requete avec la bonne valeur autfs, btrfs, tmpfs.

Vous pouvez trouver la bonne valeur pour votre système dans Prometheus en lançant cette demande : http://localhost:9090
``` yaml
node_filesystem_free_bytes
```
Pour le modifier aller dans le fichier [docker-container.json](./docker-container.json) `ligne 406`et modifier la valeur par défaut (tmpfs) par celle obtenue dans la demande sur Prometheus.

***Docker Containers Dashboard***

Le tableau de bord des conteneurs Docker affiche les mesures clés pour la surveillance des conteneurs en cours d’exécution :

* Charge totale du processeur, de la mémoire et de l’utilisation du stockage des conteneurs
* Exécution d’un graphique de conteneurs, d’un graphique de charge système, d’un graphique d’utilisation des E/S
* Graphique d’utilisation du processeur de conteneur
* Graphique d’utilisation de la mémoire du conteneur
* Graphique d’utilisation de la mémoire mise en cache du conteneur
* Graphique d’utilisation entrante du réseau de conteneurs
* Graphique d’utilisation sortante du réseau de conteneurs

Notez que ce tableau de bord n’affiche pas les conteneurs qui font partie de la pile de surveillance.

***Docker Containers Dashboard***

Le tableau de bord Monitor Services affiche des mesures clés pour la surveillance des conteneurs qui composent la pile de surveillance :

* Disponibilité du conteneur Prometheus, surveillance de l’utilisation totale de la mémoire de la pile, morceaux de mémoire de stockage local Prometheus et séries
* Graphique d’utilisation du processeur de conteneur
* Graphique d’utilisation de la mémoire du conteneur
* Morceaux de Prométhée à persister et graphiques d’urgence de persistance
* Prometheus chunks ops et graphiques de durée de point de contrôle
* Prometheus échantillonne le taux d’ingestion, les scrapes cibles et les graphiques de durée de scrape
* Graphique des requêtes HTTP Prometheus
* Graphique d’alertes Prometheus

## Définir des alertes

Trois groupes d’alertes ont été configurés dans le fichier de configuration alert.rules :

* Monitoring services alerts
* Docker Host alerts
* Docker Containers alerts

Vous pouvez modifier les règles d’alerte et les recharger en se connectant à l'adresse http://localhost:9090/alerts


***Monitoring services alerts***

Déclenchez une alerte si l’une des cibles de surveillance (node-exporter et cAdvisor) est en panne pendant plus de 30 secondes :

```yaml
- alert: monitor_service_down
    expr: up == 0
    for: 30s
    labels:
      severity: critical
    annotations:
      summary: "Monitor service non-operational"
      description: "Service {{ $labels.instance }} is down."
```

***Docker Host alerts***

Déclenchez une alerte si le processeur de l’hôte Docker est soumis à une charge élevée pendant plus de 30 secondes :

```yaml
- alert: high_cpu_load
    expr: node_load1 > 1.5
    for: 30s
    labels:
      severity: warning
    annotations:
      summary: "Server under high load"
      description: "Docker host is under high load, the avg load 1m is at {{ $value}}. Reported by instance {{ $labels.instance }} of job {{ $labels.job }}."
```
/!\ Modifiez le seuil de charge en fonction de vos cœurs de processeur.


Déclenchez une alerte si la mémoire de l’hôte Docker est presque pleine :

```yaml
- alert: high_memory_load
    expr: (sum(node_memory_MemTotal_bytes) - sum(node_memory_MemFree_bytes + node_memory_Buffers_bytes + node_memory_Cached_bytes) ) / sum(node_memory_MemTotal_bytes) * 100 > 85
    for: 30s
    labels:
      severity: warning
    annotations:
      summary: "Server memory is almost full"
      description: "Docker host memory usage is {{ humanize $value}}%. Reported by instance {{ $labels.instance }} of job {{ $labels.job }}."
```

Déclenchez une alerte si le stockage de l’hôte Docker est presque plein :

```yaml
- alert: high_storage_load
    expr: (node_filesystem_size_bytes{fstype="aufs"} - node_filesystem_free_bytes{fstype="aufs"}) / node_filesystem_size_bytes{fstype="aufs"}  * 100 > 85
    for: 30s
    labels:
      severity: warning
    annotations:
      summary: "Server storage is almost full"
      description: "Docker host storage usage is {{ humanize $value}}%. Reported by instance {{ $labels.instance }} of job {{ $labels.job }}."
```

***Docker Containers alerts***

Déclenchez une alerte si un conteneur est en panne pendant plus de 30 secondes :

```yaml
- alert: jenkins_down
    expr: absent(container_memory_usage_bytes{name="jenkins"})
    for: 30s
    labels:
      severity: critical
    annotations:
      summary: "Jenkins down"
      description: "Jenkins container is down for more than 30 seconds."
```

Déclenchez une alerte si un conteneur utilise plus de 10 % du total des cœurs de processeur pendant plus de 30 secondes :

```yaml
- alert: jenkins_high_cpu
    expr: sum(rate(container_cpu_usage_seconds_total{name="jenkins"}[1m])) / count(node_cpu_seconds_total{mode="system"}) * 100 > 10
    for: 30s
    labels:
      severity: warning
    annotations:
      summary: "Jenkins high CPU usage"
      description: "Jenkins CPU usage is {{ humanize $value}}%."
```

Déclenchez une alerte si un conteneur utilise plus de 1,2 Go de RAM pendant plus de 30 secondes :

```yaml
- alert: jenkins_high_memory
    expr: sum(container_memory_usage_bytes{name="jenkins"}) > 1200000000
    for: 30s
    labels:
      severity: warning
    annotations:
      summary: "Jenkins high memory usage"
      description: "Jenkins memory consumption is at {{ humanize $value}}."
```

## /!\ Mise à jour mot de passe Caddy /!\

**Caddy v2 n’accepte pas les mots de passe en texte brut. Il DOIT être haché. Le hachage du mot de passe ici correspond à ADMIN_PASSWORD => 'admin'**

Effectuez un `docker run --rm caddy caddy hash-password --plaintext 'ADMIN_PASSWORD'` afin de générer un hachage pour votre nouveau mot de passe. ASSUREZ-VOUS de remplacer `ADMIN_PASSWORD` par un nouveau mot de passe en texte brut et `ADMIN_PASSWORD_HASH` par le mot de passe haché référencé dans le [docker-compose.yml](./docker-compose.yml).